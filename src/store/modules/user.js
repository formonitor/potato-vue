import { login, getInfo, logout  } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/auth'

const user = {

  state: {
    token: getToken(),
    name: '',
    avatar: '',
    roles: []
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    }
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      console.log('跳入Login')
      const username = userInfo.username.trim()
      console.log('从userInfo中获取username')

      return new Promise((resolve, reject) => {

        login(username, userInfo.password).then(response => {
          console.log('login返回response')
          console.log("response:"  + response)
          //setToken(response.token)
          //commit('SET_TOKEN', data.token)
          resolve()

        }).catch(error => {
          reject(error)
          console.log(error.toString())
        })


      })


    },

    // 获取用户有哪些角色。
    GetInfo({ commit, state }) {

      return new Promise((resolve, reject) => {

        //调用getInfo
        getInfo(state.token).then(response => {
          const data = response.data

          commit('SET_ROLES', data.roles)
          commit('SET_NAME', data.name)
          commit('SET_AVATAR', data.avatar)

          resolve(response)
        }).catch(error => {
          reject(error)
        })

      })

    },

    // 登出
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '')
          commit('SET_ROLES', [])
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  }
}

export default user
