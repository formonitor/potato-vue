

var position = {

  state: {
    farmland: 'farmland1',
    shot: 'shot2',
    field: 'field3'
  },


  mutations: {
    SET_FARMLAND: (state, farmland) => {
      state.farmland = farmland
    },
    SET_SHOT: (state, shot) => {
      state.shot = shot
    },
    SET_FIELD: (state, field) => {
      state.field = field
    }
  }

}

export default position