/** When your routing table is too long, you can split it into small modules**/

import Layout from '../../views/layout/Layout'

const nestedRouter = {
  path: '/nested',
  component: Layout,
  redirect: '/nested/menu1/menu1-1',
  name: 'Nested',
  meta: {
    title: '试验田管理',
    icon: 'table'
  },
  children: [
    {
      path: 'menu1',
      component: () => import('@/views/nested/menu1/index'), // Parent router-view
      name: 'Menu1',
      meta: { title: '试验基地管理' },
      redirect: '/nested/menu1/menu1-1',
      children: [
        {
          path: '/farm',
          component: () => import('@/views/farmlands/farm'),
          name: 'farm',
          meta: { title: '试验田列表' }
        },
        {
          path: '/farmManage',
          component: () => import('@/views/farmlands/bigfarm'),
          name: '/farmMange',
          redirect: '/views/farmlands/bigfarm',
          meta: { title: '试验田管理' },
          children: [
            {
              path: '/browser',
              component: () => import('@/views/farmlands/browser'),
              name: 'brower',
              meta: { title: '种植图浏览' }
            },
            {
              path: '/greenhouseDesign',
              component: () => import('@/views/species/greenhouseDesign'),
              name: 'Greenhouse',
              meta: { title: '棚内' }
            },
            {
              path: '/plantDesign',
              component: () => import('@/views/species/plantDesign'),
              name: 'plantDesign',
              meta: { title: '棚外' }
            }
          ]
        }
      ]
    },
    {
      path: '/bigfarm',
      name: 'bigfarm',
      component: () => import('@/views/farmlands/bigfarm'),
      meta: { title: '试验基地列表' }
    }
  ]
}

export default nestedRouter
