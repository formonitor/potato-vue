import Vue from 'vue'
import Router from 'vue-router'
const _import = require('./_import_' + process.env.NODE_ENV)
Vue.use(Router)

import Layout from '../views/layout/Layout'
import nestedRouter from './modules/nested'


export const constantRouterMap = [
  { path: '/login', component: _import('login/index'), hidden: true },
  { path: '/404', component: _import('404'), hidden: true },

  //主页
  {
    path: '/',
    component: Layout,
    //redirect: '/dashboard',
    //name: 'Dashboard',
    redirect: '/homePage',
    name: 'homePage',
    hidden: true,
    children: [{
      //path: 'dashboard',
      //component: () => import('@/views/dashboard/index')}]
      path: 'homePage',
      component: () => import('@/views/farmlands/homePage') }]
  },

  //表格
  {
    path: '/example',
    component: Layout,
    redirect: '/example/table',
    name: 'Example',
    meta: { title: '试验田管理', icon: 'table' },

    children: [
      // {
      //   path: '/bigfarm',
      //   name: 'bigfarm',
      //   component: () => import('@/views/farmlands/bigfarm'),
      //   meta: { role: ['admin'], title: '大田列表' }
      // },
      {
        path: '/newfarm',
        name: 'newfarm',
        component: () => import('@/views/farmlands/newfarm'),
        meta: { title: '基地列表' }
      },
      {
        path: '/farm',
        name: 'farm',
        component: () => import('@/views/farmlands/farm'),
        meta: { role: ['admin'], title: '试验田列表' }
      },

      {
        path: '/browser',
        name: 'browser',
        component: () => import('@/views/farmlands/browser'),
        meta: { title: '种植图浏览' }
      },

      {
        path: '/plantDesign',
        name: 'planDesign',
        component: () => import('@/views/species/plantDesign'),
        meta: { title: '小区品种种植图' }
      },

      {
        path: '/homePage',
        name: 'homePage',
        component: () => import('@/views/farmlands/homePage'),
        meta: { title: '首页测试' }
      },
      {
        path: '/greenhouseDesign',
        name: 'greenhouseDesign',
        component: () => import('@/views/species/greenhouseDesign'),
        meta: { title: '棚内品种种植图' }
      }
      // {
      //   path: '/earlymaturing_identify',
      //   name: 'earlymaturing_identify',
      //   component: () => import('@/views/farmlands/experiments/earlymaturing_identify'),
      //   meta: { title: '早熟鉴定试验' }
      // },
      // {
      //   path: '/earlymaturing_preparation',
      //   name: 'earlymaturing_preparation',
      //   component: () => import('@/views/farmlands/experiments/earlymaturing_preparation'),
      //   meta: { title: '早熟预备比试验' }
      // },

      // // 示例-------------------
      // {
      //   path: 'table',
      //   name: 'Table',
      //   component: () => import('@/views/table/index'),
      //   meta: { title: 'complexTable' }
      // },
      // {
      //   path: 'fullcalendar',
      //   name: 'Fullcalendar',
      //   component: () => import('@/views/fullcalendar/fullcalendar'),
      //   meta: { title: 'calendar' }
      // }
    ]
  },
  // nestedRouter,

  {
    path: '/speciesmodule',
    component: Layout,
    redirect: '/speciesmodule/allspecies',
    name: 'species',
    meta: { title: '品种资源库', icon: 'table' },

    children: [
      {
        path: '/speciesmodule/localSpecies',
        name: 'localSpecies',
        component: () => import('@/views/species/localSpecies'),
        meta: { role: 'admin',title: '本地品种资源库' },
        children: [
          {
            path: '/speciesmodule/localSpecies',
            name: 'localSpecies',
            component: () => import('@/views/species/localSpecies'),
            meta: { role: 'admin',title: '本地品种资源库-1' },
          }

        ]
      },
      {
        path: '/speciesmodule/testIDInSpecies',
        name: 'testIDInSpecies',
        component: () => import('@/views/species/testIDInSpecies'),
        meta: { role: 'admin',title: '试验列表' }
      },

      {
        path: '/speciesmodule/speciesdetail',
        name: 'speciesdetail',
        component: () => import('@/views/species/speciesdetail'),
        meta: { title: '品种详情' }
      }
    ]
  },

  {
    path: '/experiments',
    component: Layout,
    redirect: '/earlymaturing_identify',
    name: 'experiments',
    meta: { title: '实验采集数据', icon: 'table' },

    children: [
      // {
      //   path: '/earlymaturing_identify',
      //   name: 'earlymaturing_identify',
      //   component: () => import('@/views/farmlands/experiments/earlymaturing_identify'),
      //   meta: { title: '早熟鉴定试验' }
      // },
      // {
      //   path: '/earlymaturing_preparation',
      //   name: 'earlymaturing_preparation',
      //   component: () => import('@/views/farmlands/experiments/earlymaturing_preparation'),
      //   meta: { title: '早熟预备比' }
      // },
      {
        path: '/commontest',
        name: 'commontest',
        component: () => import('@/views/farmlands/experiments/commontest'),
        meta: { title: '实验采集数据' }
      }
    ]
  },

  // {
  //   path: '/tab',
  //   name: 'tab',
  //   component: Layout,
  //   meta: { icon: 'example' },
  //   children: [
  //     {
  //       path: 'Tabs',
  //       name: 'Tabs',
  //       component: () => import('@/views/tab/index'),
  //       meta: { title: 'Tabs', icon: 'tab' }
  //     }
  //   ]
  // },

  // 表单
  // {
  //   path: '/form',
  //   component: Layout,
  //   redirect: '/table/BaseForm',
  //   name: 'form',
  //   meta: {
  //     title: 'form',
  //     icon: 'form'
  //   },
  //   children: [
  //     {
  //       path: 'Form',
  //       name: 'Form',
  //       component: () => import('@/views/form/index'),
  //       meta: { title: 'BaseForm' }
  //     },
  //     {
  //       path: 'qiniu',
  //       name: 'qiniu',
  //       component: () => import('@/views/form/qiniu'),
  //       meta: { title: 'qiniu' }
  //     },
  //     {
  //       path: 'quillEditor',
  //       name: 'quillEditor',
  //       component: () => import('@/views/form/quillEditor'),
  //       meta: { title: 'quillEditor' }
  //     },
  //     {
  //       path: 'tinymce',
  //       name: 'tinymce',
  //       component: () => import('@/views/form/tinymce'),
  //       meta: { title: 'tinymce' }
  //     }
  //   ]
  // }
]

export default new Router({
  //mode: 'history', //后端支持可开
  mode: 'hash',
  routers: [{
    path: '/',
    redirect: '/homePage'
  }],
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

export const asyncRouterMap = [
  // {
  //   path: '/icon',
  //   component: Layout,
  //   name: 'icons',
  //   meta: { roles: ['admin'] },
  //   children: [
  //     {
  //       path: 'iconIndex',
  //       name: 'iconIndex',
  //       component: () => import('@/views/svg-icon/index'),
  //       meta: { title: 'svgicons', icon: 'icon', roles: ['admin'] }
  //     }
  //   ]
  // },
  // 树形组件
  // {
  //   path: '/treeMen',
  //   component: Layout,
  //   redirect: 'noredirect',
  //   meta: {
  //     title: 'treeMen',
  //     icon: 'TreeMean'
  //   },
  //   children: [
  //     {
  //       path: 'treeMen',
  //       name: 'treeMen-demo',
  //       component: () => import('@/views/tree/index'),
  //       meta: { title: 'treeMen', icon: 'TreeMean' }
  //     }
  //   ]
  // },


  //组件
  // {
  //   path: '/components',
  //   component: Layout,
  //   redirect: '/components/dragKanban',
  //   name: 'Components',
  //   meta: {
  //     title: 'Components',
  //     icon: 'component'
  //   },
  //   children: [
  //     {
  //       path: 'componentsmixin',
  //       name: 'mixin',
  //       component: () => import('@/views/components/backToTop'),
  //       meta: { title: 'backToTop' }
  //     },
  //     {
  //       path: 'mixin',
  //       name: 'componentMixin',
  //       component: () => import('@/views/components/mixin'),
  //       meta: { title: 'componentMixin' }
  //     }
  //   ]
  // },
  // {
  //   path: '/excel',
  //   component: Layout,
  //   redirect: '/excel/exportExcel',
  //   name: 'excel',
  //   meta: {
  //     title: 'excel',
  //     icon: 'excel'
  //   },
  //   children: [
  //     {
  //       path: 'exportExcel',
  //       name: 'exportExcel',
  //       component: () => import('@/views/excel/exportExcel'),
  //       meta: { title: 'exportExcel', icon: 'excel' }
  //     }
  //   ]
  // },
  // {
  //   path: '/i18n-demo',
  //   component: Layout,
  //   redirect: 'i18n-demo',
  //   children: [
  //     {
  //       path: 'indexLang',
  //       name: 'indexLang',
  //       component: () => import('@/views/i18n-demo/indexLang'),
  //       meta: { title: 'i18n', icon: 'international' }
  //     }
  //   ]
  // },
  { path: '*', redirect: '/404', hidden: true }
]
