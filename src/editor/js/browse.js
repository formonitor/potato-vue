$(function () {
    var link = 'http://120.78.130.251:9527';
    var container=document.getElementById('browse-container');
    var parent = document.getElementById("parent");
    var myGrids = new Array();
    var len, wid //容器长宽(m)
    var h;
    var w;//容器高宽（px）
    // var date = new Date()
    // var current_year = date.getFullYear()
    // var year = 2018
    var line_spacing = 0.65 // 行距
    var row_spacing = 0.3 // 株距

    var url = decodeURI(window.location.href);
    //console.log(url)
    var argsIndex = url .split("?farmlandId=");
    var str = argsIndex[1];
    //var string = window.location.search
    //var loc = location.href;
    //var n1 = loc.length;//地址的总长度
    //var n2 = loc.indexOf("=");//取得=号的位置
    //var str0 = decodeURI(loc.substr(n2+1, n1-n2));//从=号后面的内容
    //var n3 = str0.indexOf('&');
    //var year = str0.substr(0, n3);
    //var str = string.split('=')[1]
    console.log(str);

    $.ajax({
      type: 'POST',
      url: link+'/farm/getFarmList',
      contentType: 'application/json; charset=utf-8',
      data: JSON.stringify({ 'farmlandId': str }),
      dataType: 'json',
      success: function(data) {
        console.log('success')
        len=data.rows[0].length;
        wid=data.rows[0].width;
        console.log(wid) //宽度
        console.log(len) //高度
        w=parseInt(wid/row_spacing/5*50)
        h=parseInt(len/line_spacing*20)

        parent.style.height = 0.5*h + "px";
        parent.style.width = 0.5*w + "px";
        container.style.height = 0.5*h + "px";
        container.style.width = 0.5*w + "px";
        reading(w);
      },
      error: function(message) {
        console.log("error")
      }
   })

  var type = getExpType()

    function reading(w){
      $.ajax({
        url: link+'/experimentfield/getExperimentFieldList',
        type: 'POST',
        contentType: 'application/json; charset=UTF-8',
        async: false,
        dataType: 'json',
        data: JSON.stringify({ 'farmlandId': str }),
        success: function(data) {
          console.log(data)
          var $jsontip = $('#jsonTip')
          // var strHtml = ''
          // 存储数据的变量
          $jsontip.empty()
          // 清空内容
          number = data.total
          var json = data.rows
          var p = 0
          for (var i = 0; i < number; i++) {
            var plotId = json[i].id
            var species_num = json[i].num
            var regular = json[i].expType
            for (var q = 0; q < type.length; q++) {
              if (regular == type[q]['exType']) {
                p = q
              } else {

              }
            }
            // regular = regular.slice(1, regular.length - 1).split(',')
            // var in_row=$("#in_row").val();
            // var in_col=$("#in_col").val();

            var in_row = parseFloat(type[p]['row']) // 行 占几行
            var in_col = parseInt(type[p]['repeat']) // 重复
            var zhu = parseInt(type[p]['zhu']) / 5 // 株/5 占几格
            var expType = regular
            var color = type[p]['color']
            console.log(expType)
            var width = 10;
            var height = 15;
            width = (width + 1) * zhu;
            height = (height + 0.8) * species_num * in_row/3.5;

            //console.log("0=====>"+width+height);
            // 创建土地区域
            var area = document.createElement('ul')

            area.setAttribute('id', plotId)
            area.setAttribute('name', expType)
            area.setAttribute('color', color)
            area.setAttribute('sNum', species_num)
            area.classList.add('in_p', 'in', 'draggable', 'ui-widget-content')
            area.style.color = color;
            area.style.height = height + 'px';
            area.style.width = width + 'px';
            document.getElementById('browse-container').appendChild(area);

            area.style.marginTop = parseInt(json[i].moveY) + 'px'
            // area.style.marginBottom =(parseInt(json["move_y1"])+2)+'px';
            // area.style.marginLeft = parseInt(json["moveX"])+'px';

            area.style.marginRight = (w - parseInt(json[i].moveX1)) + 'px'
            // var color=getRandomColor();
            for (var j = 0; j < in_col; j++) {
              var t = document.createElement('li')
              var text = document.createTextNode(regular + ' ' + species_num + '品种')
              var span = document.createElement('span')
              span.style.color = '#000000'
              span.appendChild(text)
              t.appendChild(span)

              t.style.width = width + 'px'
              t.style.height = height + 'px'
              t.style.backgroundColor = color
              if (j === 0) {
                t.style.marginRight = 0 + 'px';
              }
              area.appendChild(t)
            }
            //area.style.transform = 'scale(0.5,0.5)';
            myGrids.push(plotId)
          }
        }
      })

      container.style.transform = 'scale(0.4,0.6)translate(500px, -1000px)';

    }

   // container.style.transform = 'translate(500px,500px)';
    // container.style.left=10+"px";
    // container.style.top=10+"px";
    //container.style.width = w;
    //container.style.height = h;
    // console.log(size_w,size_h);
    //container.scale(size_w,size_h);

  function getExpType() {
    var expType = new Array()
    $.ajax({
      type: 'GET',
      url: 'myExps.json',
      dataType: 'json',
      // data: null,
      async: false,
      success: function(data) {
        expType = data
      }
    })
    console.log(expType)
    return expType
  }


});
