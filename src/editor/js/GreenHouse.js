$(function () {
  var length,width;
  var line_spacing = 0.65 // 行距
  length = 10;
  var l = length/line_spacing; //行数
  var parent = document.getElementById('parent')
  var columns = 3
  //parent.setAttribute('style', 'grid-template-columns:repeat(' + columns + ',120px)')
  parent.setAttribute('style', 'grid-template-columns: 200px 40px 200px')
  for (var i = 0; i < l; i++) {
    for (var j = 0; j < columns; j++) {
      var x = document.createElement('div')
      if(j==1){
        x.classList.add('col2')
        x.style.alignItems="center"
        parent.appendChild(x)
      }
      else{
        x.classList.add('col1')
        //x.setAttribute('flag', i + ' ' + j)
        x.innerHTML=(i+1)
        parent.appendChild(x)
      }


    }
  }
  var son = document.getElementById('containment-wrapper')
  son.style.width = 1000 + 'px' // 框架宽高

  son.style.height = 1000 + 'px'
  $('.col1').css({ 'width': '200px', 'height': '40px','background': '#ccc' ,"text-align":"center"})
  $('.col2').css({ 'width': '40px', 'height': '40px','background': '#000000' ,"text-align":"center" })
})
