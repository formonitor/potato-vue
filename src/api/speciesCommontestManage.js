// V2.0 speciescommontest 相关接口

import request from '@/utils/request'
export function addCommontest(params) {
  return request({
    url: '/SpeciesCommontest/addCommontest',
    method: 'post',
    data: params
  })
}

export function deleteCommontest(params) {
  return request({
    url: '/SpeciesCommontest/deleteCommontest',
    method: 'post',
    params
  })
}


export function getSpeciesCommontestList(testId) {
  return request({
    url: '/SpeciesCommontest/getSpeciesCommontestList' + '?testId=' + testId,
    method: 'post'
  })
}

export function updateCommontest(params) {
  return request({
    url: '/SpeciesCommontest/updateCommontest',
    method: 'post',
    data: params
  })
}
