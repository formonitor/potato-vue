import request from '@/utils/request'
//1-获取[所有/类似某id]的species
export function getSpeciesList(params) {
  return request({
    url: '/species/getSpeciesList',
    method: 'post',
    params
  })
}

//1-添加species，
export function addSpecies(params) {
  return request({
    url: '/species/addSpecies',
    method: 'post',
    params
  })
}

//1-获取指定speciesId的speciesDetail
export function getSpeciesDetail(params) {
  return request({
    url: '/species/getSpeciesDetail',
    method: 'post',
    params
  })
}
//1-更新指定speciesId的speciesDetail；
export function updateSpeciesDetail(params) {
  return request({
    url: '/species/updateSpeciesDetail',
    method: 'post',
    data: params
  })
}

//1-更新指定fieldId的早熟预备比项目列表
export function getEarlymaturingPreparationItemList(params) {
  return request({
    url: '/species/getEarlymaturingPreparationItemList',
    method: 'get',
    data: params
  })
}

// V2.0 条件查询本地种子资源库
export function addLocalSpecies(params) {
  return request({
    url: '/localspecies/addLocalSpecies',
    method: 'post',
    data: params
  })
}

export function deleteLocalSpecies(localSpeciesId) {
  return request({
    url: '/localspecies/deleteLocalSpecies' + '?localSpeciesId=' + localSpeciesId,
    method: 'post',
  })
}

export function getLocalSpecies(params) {
  return request({
    url: '/localspecies/getLocalSpecies',
    method: 'post',
    data: params
  })
}

export function updateLocalSpecies(params) {
  return request({
    url: '/localspecies/updateLocalSpecies',
    method: 'post',
    data: params
  })
}

export function updateSpeciesPic(params) {
  return request({
    url: '/localspecies/updateSpeciesPic',
    method: 'post',
    data: params
  })
}
