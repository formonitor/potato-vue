import request from '@/utils/request'

// 01-根据fieldID获取其包含的speciesId信息
export function getInnerSpeciesByFieldId(fieldId) {
  return request({
    // url: '/Block/getInnerSpeciesByFieldId',
    url: '/Block/getInnerSpeciesByFieldId' + '?fieldId=' + fieldId,
    method: 'post'
  })
}

// 01-根据fieldID获取其包含的blockId信息
export function getInnerBlockByFieldId(fieldId) {
  return request({
    url: '/Block/getInnerBlockByFieldId' + '?fieldId=' + fieldId,
    method: 'post'
  })
}

// 02-为指定的试验田(fieldId)存放其所包含的品种信息(speciesId数组)
export function putInnerSpeciesWithExp(params) {
  return request({
    url: '/Block/putInnerSpeciesWithExp',
    method: 'post',
    data: params
  })
}

// 03-02方法的批量化
export function putInnerSpeciesWithExpList(params) {
  return request({
    url: '/Block/putInnerSpeciesWithExpList',
    method: 'post',
    data: params
  })
}
export function putSpeciesPositionToBlock(params) {
  return request({
    url: '/Block/putSpeciesPositionToBlock',
    method: 'post',
    data: params
  })
}
