import request from '@/utils/request'

// 1-添加试验田
export function addFarm(params) {
  return request({
    url: '/farm/addFarm',
    method: 'post',
    data: params
  })
}

// 2-删除试验田
export function deleteFarm(params) {
  return request({
    url: '/farm/deleteFarm',
    method: 'post',
    params
  })
}

// 3-条件查询获取farm列表
export function getFarmList(params) {
  return request({
    url: '/farm/getFarmList',
    method: 'post',
    data: params
  })
}

// 05-查询某bigfarm下所有的farm
export function getFarmListByBigfarmId(bigfarmId) {
  return request({
    url: '/bigfarm/getFarms',
    method: 'get',
    bigfarmId
  })
}

// 4-更新farm信息
export function updateFarm(farm) {
  return request({
    url: '/farm/updateFarm',
    method: 'post',
    farm
  })
}
