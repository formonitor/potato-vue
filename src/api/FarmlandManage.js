import request from '@/utils/request'

// 1-获取大田列表
export function getAllFarmland(params) {
  return request({
    url: '/getAllFarmland',
    method: 'get',
    params
  })
}
// 4.
export function getExperimentTypes(params) {
  return request({
    url: '/getExperimentTypes',
    method: 'get',
    params
  })
}
// 2.1-删除大田
export function deleteFarmland(params) {
  return request({
    url: '/deleteFarmland',
    method: 'post',
    params
  })
}
// 3-
export function getShotList(params) {
  return request({
    url: '/getShotList',
    method: 'post',
    params
  })
}

// 3.1-
export function getFieldList(params) {
  return request({
    url: '/getFieldList',
    method: 'post',
    params
  })
}

// 3.1-
export function getPlotList(params) {
  return request({
    url: '/getPlotList',
    method: 'post',
    params
  })
}

// 3.x- 新增大田信息
export function addFarmland(params) {
  return request({
    url: '/addFarmland',
    method: 'post',
    params
  })
}
// 5-添加试验田
export function addField(params) {
  return request({
    url: 'addField',
    method: 'post',
    params
  })
}
// 5.1-编辑试验田名称和备注
export function editField (params) {
  return request({
    url: '/editField',
    method: 'post',
    params
  })
}
// 5.2-删除试验田
export function deleteField(params) {
  return request({
    url: '/deleteField',
    method: 'post',
    params
  })

}

// 5.3-删除试验田
export function addPlot(params) {
  return request({
    url: '/addPlot',
    method: 'post',
    params
  })

}

// 获取plot的试验数据
export function getPlotData(params) {
  return request({
    url: '/getPlotData',
    method: 'post',
    params
  })
}

// 更新plot的试验数据
export function updatePlotData(params) {
  return request({
    url: '/updatePlotData',
    method: 'post',
    data:params
  })
}

export function addFarm(params) {
  return request({
    url: '/farm/addFarm',
    method: 'post',
    data: params
  })
}
