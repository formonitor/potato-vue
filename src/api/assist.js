import request from '@/utils/request'

// 1-获取某一年份的farmTree
export function getFarmTree(year) {
  return request({
    url: '/Assist/getFarmTree' + '?year=' + year,
    method: 'get',
  })
}
