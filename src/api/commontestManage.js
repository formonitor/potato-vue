// V2.0 条件查询本地种子资源库

import request from '@/utils/request'
export function addCommontest(params) {
  return request({
    url: '/Commontest/addCommontest',
    method: 'post',
    data: params
  })
}

export function deleteLocalcommontest(params) {
  return request({
    url: '/Commontest/deleteLocalSpecies',
    method: 'post',
    params
  })
}

export function getCommontestList(params) {
  return request({
    url: '/Commontest/getCommontestList',
    method: 'post',
    data: params
  })
}

export function updateCommontest(params) {
  return request({
    url: '/Commontest/updateCommontest',
    method: 'post',
    data: params
  })
}


