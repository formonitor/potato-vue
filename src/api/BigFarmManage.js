import request from '@/utils/request'

// 1-添加一条bigfarm记录
export function addBigfarm(params) {
  return request({
    url: '/bigfarm/addBigfarm',
    method: 'post',
    data: params
  })
}

// 2-根据bigfarmId，删除对应的bigfarm
export function deleteBigfarm(bigfarmId) {
  return request({
    url: '/bigfarm/deleteBigfarm' + '?bigfarmId=' + bigfarmId,
    method: 'post',
  })
}

// 3-获取所有的bigfarm
export function getAllBigfarm(params) {
  return request({
    url: '/bigfarm/getAllBigfarm',
    method: 'post',
    params
  })
}

// 4-按名称查找bigfarm
export function getBigfarmByName(name) {
  return request({
    url: '/bigfarm/getBigfarmByName' + '?name=' + name,
    method: 'post'
  })
}

// 5-按id查找bigfarm
export function getBigfarm(bigfarmId) {
  return request({
    url: '/bigfarm/getBigfarm' + '?bigfarmId=' + bigfarmId,
    method: 'post'
  })
}

export function getFarms(bigfarmId) {
  return request({
    url: '/bigfarm/getFarms' + '?bigfarmId=' + bigfarmId,
    method: 'get'
  })
}
export function getGreenhouses(bigfarmId) {
  return request({
    url: '/bigfarm/getGreenhouses' + '?bigfarmId=' + bigfarmId,
    method: 'get'
  })
}

export function getFieldPlan(bigfarmId) {
  return request({
    url: '/bigfarm/getFieldPlan' + '?bigfarmId=' + bigfarmId,
    method: 'get'
  })

}

export function getBigfarmByYear(year) {
  return request({
    url: '/bigfarm/getBigfarmByYear' + '?year=' + year,
    method: 'post'
  })

}
