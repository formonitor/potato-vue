import request from '@/utils/request'

// 1-添加field详情
export function addExperimentField(params) {
  return request({
    url: '/experimentfield/addExperimentField',
    method: 'post',
    data: params
  })
}

// 2-添加fieldList详情
export function addExperimentFieldList(params) {
  return request({
    url: '/experimentfield/addExperimentFieldList',
    method: 'post',
    data: params
  })
}

// 3-删除field详情
export function deleteExperimentField(params) {
  return request({
    url: '/experimentfield/deleteExperimentField',
    method: 'post',
    data: params
  })
}

// 4-条件查询field详情
export function getExperimentFieldList(params) {
  return request({
    url: '/farm/getExperimentFields',
    method: 'get',
    params
  })
}

export function getBlockListOfFieldType(params) {
  return request({
    url: '/experimentfield/getBlockListOfFieldType',
    method: 'get',
    params
  })
}

export function getExperimentField(params) {
  return request({
    url: '/experimentfield/getExperimentField',
    method: 'get',
    params
  })
}
