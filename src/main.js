import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en'
import 'fullcalendar/dist/fullcalendar.min.css'
import '@/styles/index.scss' // global css
import App from './App'
import router from './router'
import store from './store'
import '@/icons' // icon
import '@/permission' // permission control权限控制；
import i18n from './lang'
import FullCalendar from 'vue-full-calendar'
import VueResource from 'vue-resource'


// 图片点击放大悬浮测试
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'

import global from '@/global'


Vue.use(VueResource)
Vue.use(FullCalendar)
Vue.use(ElementUI, { locale })
Vue.use(ElementUI, {
  size: 'medium', // set element-ui default size
  i18n: (key, value) => i18n.t(key, value)
})
Vue.use(ElementUI)

Vue.config.productionTip = false
Vue.prototype.GLOBAL = global
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  template: '<App/>',
  components: { App }
})

Vue.use(Viewer)
Viewer.setDefaults({
  Options: { "inline": true, "button": true, "navbar": true, "title": true, "toolbar": true, "tooltip": true, "movable": true, "zoomable": true, "rotatable": true, "scalable": true, "transition": true, "fullscreen": true, "keyboard": true, "url": "data-source" }
});


